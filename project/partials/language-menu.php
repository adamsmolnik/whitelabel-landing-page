<div id="jsLanguageSelectorPopout" class="timeline-control-language__popout">

    <div id="jsLanguageMenuTitle" class="language-menu-title" tabindex="2">
        <div class="float-left">
            <div class="setting-arrow left"></div>
        </div>
        <div class="language-menu-text setting-text language">
            <span class="translate" data-translate="LanguageMenuText">Language</span>
            <span class="setting-light-text js-language-text"> (EN)</span>
        </div>
        <div class="clear-both"></div>
    </div>

    <div class="timeline-control-language__item" data-language="en" tabindex="2">
        <div class="language-tick-container">
            <div class="selected-lang">✔</div>
        </div>
        <div class="language-item-text">English (EN)</div>
        <div class="clear-both"></div>
    </div>
    <div class="timeline-control-language__item" data-language="es" tabindex="2">
        <div class="language-tick-container">
            <div class="selected-lang">✔</div>
        </div>
        <div class="language-item-text">Espa&ntilde;ol (ES)</div>
        <div class="clear-both"></div>
    </div>
</div>